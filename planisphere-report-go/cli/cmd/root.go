/*
Package cmd is the command line app for reporting
*/
package cmd

import (
	"log/slog"
	"os"
	"path"
	"strings"
	"time"

	"github.com/charmbracelet/log"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"

	"github.com/spf13/viper"
)

var cfgFile string

var (
	planisphereKey string
	planisphereURL string
	version        = "dev"
	startedAt      time.Time
	logger         *slog.Logger
)

// Verbose Logging
var Verbose bool

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "planisphere-report",
	Short:   "Self report tool for Planisphere",
	Long:    `Self report tool for Planisphere. Currently supported on macOS, Linux and FreeBSD`,
	Version: version,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
	PersistentPreRun: func(_ *cobra.Command, _ []string) {
		// Key is gonna be required
		startedAt = time.Now()
		planisphereKey = viper.GetString("key")
		if planisphereKey == "" {
			logFatal("Must set your Planisphere Key in the config file or env. See README.md for full details. Shortcut to link to your keys: https://duke.is/vywtw", nil)
		}
		logger.Debug("using key", "key", planisphereKey)

		// Set URL if needed
		planisphereURL = viper.GetString("url")
		if planisphereURL == "" {
			planisphereURL = "https://planisphere.oit.duke.edu/self_report"
		}
		logger.Debug("using url", "url", planisphereURL)
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.planisphere-report.yaml)")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "Enable verbose output")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func initLogging() {
	lopts := log.Options{
		Prefix:          "🕰 planisphere-report ",
		ReportTimestamp: true,
		TimeFormat:      time.Kitchen,
	}
	if Verbose {
		lopts.Level = log.DebugLevel
	}
	logger = slog.New(log.NewWithOptions(os.Stderr, lopts))
	slog.SetDefault(logger)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	initLogging()
	initViper()
}

func initViper() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Now look in /etc
		viper.AddConfigPath("/etc/")
		viper.SetConfigName("planisphere-report")
		err = viper.ReadInConfig()
		logger.Debug("error reading config", "error", err)

		// Search config in home directory with name ".planisphere-report" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".planisphere-report")
		err = viper.MergeInConfig()
		logger.Debug("error doing mergeconfig", "error", err)

		// If no key is yet set, load it in from a file
		if viper.GetString("key") == "" {
			possibleKeyFiles := []string{"/etc/planisphere_key_file", "/etc/planisphere-report-key"}
			for _, pkf := range possibleKeyFiles {
				dat, err := os.ReadFile(path.Clean(pkf))
				if err == nil {
					viper.Set("key", strings.TrimSpace(string(dat)))
					break
				}
			}
		}
		if viper.GetString("key") == "" {
			logFatal(`Could not find your planisphere key. Please set it in one of the following ways:

In your planisphere-report.yaml file, use:

Option 1:

---
key: your-key

Option 2:

Set the key contents in either  /etc/planisphere_key_file or /etc/planisphere-report-key

Option 3:

Use an environment variable like:

$ export PLANISPHERE_REPORT_KEY=your-key`, nil)
		}
	}

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvPrefix("planispherereport")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		logger.Debug("using config file", "file", viper.ConfigFileUsed())
	}
}

func logFatal(msg string, err error) {
	if err != nil {
		logger.Error(msg, "error", err)
	} else {
		logger.Error(msg)
	}
	os.Exit(2)
}
