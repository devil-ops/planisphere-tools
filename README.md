# Planisphere Tools

## Planisphere Report

Planisphere Report is an Open Source tool you can use for meeting the Endpoint Management policy.

*Note that you will also still need Crowdstrike installed.*

## Planisphere Report for Windows

This is a PowerShell script that you can use to send required data up to
Planisphere. Further instructions [here](./planisphere-report-ps/README.md)

## Planisphere Report for Linux/macOS/FreeBSD

This is a go program, packaged up as a binary or deb/rpm/pkg. Further
instructions [here](./planisphere-report-go/README.md)

## Planisphere Report Python - *DEPRECATED*

This is the original script that did the planisphere reporting. Super awesome,
but still in python2, so we are trying to migrate users of this over to the go
version above. Futher instructions [here](./planisphere-report-python/README.md)

# Contributing

Do you have a use case that isn't covered by the existing solutions? We would
love to hear back from you. Feel free to submit issues or even merge request.
Thanks for helping to keep Duke safe! ❤️
